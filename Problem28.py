# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
# Problem Link : https://projecteuler.net/problem=28
#==============================================================================
# Steps :
#     1. add the odds in steps of evens
#     2. increase the evens after every 4th count
#==============================================================================
start = 1
add = 0
for i in xrange(1,501):
    for j in xrange(4):
        start += 2*i
        add += start
        continue
print start,(add + 1)