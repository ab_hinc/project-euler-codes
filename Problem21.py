# -*- coding: utf-8 -*-
"""
Created on Mon Jul 11 18:59:36 2016

@author: demerzel
"""
# Problem Link : https://projecteuler.net/problem=21
#==============================================================================
# Sum of amicable numbers
# Amicable numbers are number pairs whose sum of even divisors equals the other number in the pair
#==============================================================================

def factors(n):
    return sorted(set(reduce(list.__add__,([i, n//i] for i in xrange(1, int(n**0.5) + 1) if n % i == 0))))
#==============================================================================
def sod(k):
    m = factors(k)
    m.remove(k)
    return sum(m)
#==============================================================================
k = 0
for i in xrange(4,10000):
    e = sod(i)
    if sod(e) == i and i != e:
        k += i + e
    else: continue
print k/2 #Numbers get added twice, once for i and once e since they come in pairs