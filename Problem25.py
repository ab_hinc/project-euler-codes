# -*- coding: utf-8 -*-
"""
Created on Thu Jun  9 23:15:12 2016

@author: demerzel
"""
# Problem Link : https://projecteuler.net/problem=25
import math
gr = (1 + math.sqrt(5))/2
md = ((999 * math.log(10))+(0.5 * math.log(5)))/math.log(gr)
dig = math.ceil(md)
print dig, "is the index of the first term of Fibonacci sequence that contains 1000 digits"