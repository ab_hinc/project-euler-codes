# -*- coding: utf-8 -*-
"""
Created on Wed Jul 27 09:06:34 2016

@author: demerzel
"""
#==============================================================================
# Question:
# A permutation is an ordered arrangement of objects. For example, 3124 is one possible permutation of the digits 1, 2, 3 and 4. If all of the permutations are listed numerically or alphabetically, we call it lexicographic order. The lexicographic permutations of 0, 1 and 2 are:
#
# 012   021   102   120   201   210
#
# What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
#==============================================================================
# There are 10 digits, the number of permutations would be factorial of 10.
# First digit, if set as 0, the resulting number of permutations is factorial of 9
from math import factorial
import string
nse = string.digits
num = ''
k1 = 1000000
for i in range(9,-1,-1):
    print nse
    num1 = k1 / factorial(i)
    if k1 % factorial(i) != 0:
        num += str(nse[num1])
        nse  = string.replace(nse,nse[num1],'')
        print num1,i
        l1 = k1 - (num1*factorial(i))
        print l1
        k1 = l1
        continue
    else:
        num += str(nse[num1-1])
        nse  = string.replace(nse,nse[num1-1],'')
        print num1,i
        l1 = k1 - (num1*factorial(i))
        print l1
        k1 = l1
        continue
print num